from setuptools import setup

install_requires = [
    'redis>=3.0.0',
    'schedule==0.6.0',
    "pysnooper==0.1.0"
]

setup( 
    name='data-collector',
    version='2.8.3',
    description='Collector for local network performance data',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com:mvimplis2013/local-data-collector',
    license='Apache license 2.0',
    packages=['datacollector'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'data-collector=datacollector.control_unit:main',
        ]
    }, 
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: System :: Network :: Monitoring',
    ],
    zip_safe=True
)