# First remove previous image and avoid docker complaining that an image with same name exists
docker rm data-collector-app
docker rmi data-collector 

# Check how to package the application ... two options available either PIP or setuptools
if [ -z "$1" ]; then 
  echo "Please define packaging tool .. High-level 'pip' Or ... Low-level 'setuptools'"
  return
elif [ $1 = "pip" ]; then
  # Use PIP tool for requiremnents install
  echo "Ready to build a docker-image of python-application with pip"
  docker build -f Dockerfile.pip -t data-collector .
elif [ $1 = "setuptools" ]; then 
  echo "Ready to build a docker-image of python-application with setuptools"
  # Use setuptools for requirements install
  docker build  -f Dockerfile.setuptools -t data-collector .
else
  echo "Please, call using correct form: source build_collector.sh pip|setuptools"
fi 
