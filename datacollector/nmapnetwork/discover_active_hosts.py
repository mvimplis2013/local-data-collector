import datetime

from . import *

from .constants import HOST_DISCOVERY_DICT 
from .nmap_command import NMapCommandInSubprocess

def open_hosts_now():
    command_base = HOST_DISCOVERY_DICT['command']
    network_cidr = HOST_DISCOVERY_DICT['cidr']

    command_str = command_base + " " + network_cidr

    # Run Linux Command in Subprocess
    start = datetime.datetime.now()
    logger.debug("***** Ready to Send Torpedo to Target Board ... %s @ %s !!!", command_str, start) 

    nmap_command = NMapCommandInSubprocess(command_str)
    nmap_command.run_linux_command_blocking()
    
    # NMAP Response .. utf-8
    whole_response_string_utf8 = str(nmap_command)

    # Process command output and prepare list of active servers
    rlines = nmap_command.get_list_of_answers_for_examined_nodes()

    hostsUp_list = []
    for i in range( 0, len(rlines) ):
        #logger.debug("+++++++++++++++ %s", rlines[i])
        if rlines[i].strip().startswith('Host is up'):
            # ***************
            #  Found Host Up
            # ***************
            #logger.debug("** #%d: \n\t %s \n\t %s", i, rlines[i], rlines[i-1])

            hostsUp_list.append( rlines[i-1] )
    
    # How many Hosts-Up found ?
    #logger.debug(hostsUp_arr)
    logger.debug("Number of Hosts-Up Found = %d", len(hostsUp_list))
    logger.debug("Elapsed Time ... %s *****", datetime.datetime.now()-start)

    # Get a list of kines 'Host is Up'
    return hostsUp_list

    # Return NMAP Response ... Total   
    #return whole_response_string_utf8

