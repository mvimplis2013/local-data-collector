import schedule
import time 
import os 

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

from .host_discovery_request import perform_host_discovery
from .port_scanning_request import perform_port_scanning

"""
Search for Hosts-Up using a loose schedule
"""
def simple_job_host():
    perform_host_discovery()
    
"""
Search for Open-Ports using a frequent time scheme 
"""
def simple_job_port():
    perform_port_scanning()
    
def simple_test( freq_host_value, freq_host_units, freq_port_value, freq_port_units ):
    # PRINT() ... It is not working in case of subprocess-context
    #print("Simple Scheduler Land")
    if (freq_host_units == "MINUTES"):
        logger.debug("Ready to Schedule Frequent Host-Discovery .... Every %d minute(s)", freq_host_value)
        schedule.every( freq_host_value ).minutes.do(simple_job_host)
    elif (freq_host_units == 'HOURS'):
        logger.debug("Ready to Schedule Infrequent Host-Discovery .... Every %d hour(s)", freq_host_value)
        schedule.every( freq_host_value ).hours.do(simple_job_host)
    else:
        raise NotImplementedError("Try Another Unit for Frequency of Host Discovery")

    if (freq_port_units == "MINUTES"):
        logger.debug("Ready to Schedule Frequent Port-Scanning .... Every %d minute(s)", freq_port_value)
        schedule.every( freq_port_value ).minutes.do(simple_job_port)
    elif (freq_port_units == 'HOURS'):
        logger.debug("Ready to Schedule Infrequent Port-Discovery .... Every %d hour(s)", freq_port_value)
        schedule.every( freq_port_value ).hours.do(simple_job_port)
    else:
        raise NotImplementedError("Try Another Unit for Frequency of Port Scanning")

    while True:
        schedule.run_pending()
        time.sleep(1)

def scheduler_main():
    print("Lions Gate")

    HOST_DISCOVERY_FREQ = os.environ[ 'HOST-DISCOVERY-FREQ']
    freq_host = HOST_DISCOVERY_FREQ.split("-")
    freq_host_value = int( freq_host[0] )
    freq_host_units = str( freq_host[1] )

    PORT_SCANNING_FREQ = os.environ[ 'PORT-SCANNING-FREQ']
    freq_port = PORT_SCANNING_FREQ.split("-")
    freq_port_value = int( freq_port[0] )
    freq_port_units = str( freq_port[1] )

    logger.debug( "Host-disc-freq = %s - %s ... Port-scan-freq = %s - %s", freq_host[0], freq_host[1], freq_port_value, freq_port_units )

    simple_test( freq_host_value, freq_host_units, freq_port_value, freq_port_units )