import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

import os 
NMAP_AGENT_IP = os.environ["POKY-NMAP-SERVER"]
NMAP_AGENT_PORT = os.environ["POKY-NMAP-PORT"]

# All entries in __all__ ... must be STRING    
__all__ = ["logger", "NMAP_AGENT_IP", "NMAP_AGENT_PORT"]