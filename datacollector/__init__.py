import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

import datetime
start_time = datetime.datetime.now()

from .scheduler.host_discovery_request import perform_host_discovery
from .scheduler.redis_communication_manager import check_hosts_up_existence, get_all_hosts_up_from_datastore, store_hosts_up, store_open_ports
from .scheduler.port_scanning_request import perform_port_scanning
from .scheduler.simple_scheduler import scheduler_main

# All entries in __all__ ... must be STRING    
__all__ = ["logger", "start_time", "perform_host_discovery", 
            "check_hosts_up_existence", "get_all_hosts_up_from_datastore", "perform_port_scanning", 
            "store_hosts_up", "store_open_ports", "scheduler_main", 
          ]