import pysnooper

import time 

from . import *

from datacollector.messageqclient import *

from datacollector.scheduler.redis_communication_manager import set_open_ports_of_server, get_open_ports_of_server_from_datastore, store_hosts_up, delete_net_data_from_local_store, \
  get_list_of_keys_in_db

from datacollector.mastermind.vlab.monitoring import *

#class LocalAgent(object):
#  def __init__(self):
#    pass

import redis 

#redis_host = 'ca_baffin_island_redis_app'
redis_host = 'adoring-coris'
#redis_host = 'localhost'

redis_port = 6379

redis_password = 'antartica_total_population_is_1,106'

def run_redis_test():
  # Test Connection with Redis Database

  # Create connection object
  try: 
    #r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
    r = redis.Redis(host=redis_host, port=redis_port, db=0)

    # Set the Collector Hello message to Redis
    r.set( 'msg:CollectorHelloMsg', 'Data Collector of Local Agent is Saying Hello to Redis Memory Data Store' )

    # Retrieve the message from Redis
    msg = r.get( 'msg:CollectorHelloMsg' )

    print(msg)
  except Exception as e:
    print(e)

# Step 1 ... If datastore not available ABORT
def check_datastore():
  check_datastore_availability()

# Step 2 ... If missing HostsUp set ... Immediately call NMAP for Host Scanning
def check_hostsUp_set():
  exists = check_key_existence("hostsUp")
  return exists
  
#####
## Application Entrypoint
###
#@pysnooper.snoop()
def main():
  logger.debug("Ready to Start Monitoring Data-Collector at ... %s", start_time)

  # Step 1: Check Redis Availability (In case of No Redis Available an Exception is Thrown)
  # ------
  check_datastore()
  # ************************************
  # Should I clean the datastore first ?
  # ************************************
  delete_net_data_from_local_store()

  #set_open_ports_of_server()
  #logger.debug( "----> %s", get_open_ports_of_server() )
  
  ### EndOF Step 1 !!!

  # Step 2: Let the application check whether ...
  # ------
  #    --> the "HostsUp" data is stored at Redis
  #    --> in case of no HostsUp data this is the first execution cycle and data needs to be gathered first 
  if check_hosts_up_existence():
    # ###
    # HostsUp Found in Redis ! 
    logger.debug("NMap already performed Host-Discovery ... Stored Results Found")

    hosts_book = get_all_hosts_up_from_datastore()
    #logger.debug("Found in Datastore Hosts-Up ... %s", hosts_book)
  else:
    # ###
    # No HostsUp Found in Redis .. Need to Send Immediately a Request to Nmap for Local Network Scanning
    logger.debug("No Host-Discovery Results stored ... Sending Immediately Request to NMAP")
    hosts_book = perform_host_discovery()

    store_hosts_up( hosts_book )
  ### EndOF Step 2: Hosts-Up {} are available

  # Step 3: AFter having a long list of hosts_up, it is time to examine open-ports in each of the servers
  # ------
  ports_book = perform_port_scanning()
  
  store_open_ports( ports_book )
  get_list_of_keys_in_db();
  
  # Start scheduler
  scheduler_main()


  