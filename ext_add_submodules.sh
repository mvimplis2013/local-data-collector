# All external submodules are placed inside the "ext" subfolder
mkdir ext
cd ext

# Add Submodule TCP-Client-Server
git submodule add git@gitlab.com:mvimplis2013/tcp-client-server.git tcp-client-server
git ls-files --stage tcp-client-server

# Add Submodule Redis-Client
git submodule add git@gitlab.com:mvimplis2013/messageQ-client.git messageq-client
git ls-files --stage messageq-client

# Add Submodule Mastermind
git submodule add git@gitlab.com:mvimplis2013/master-mind.git master-mind
git ls-files --stage master-mind

git submodule update --init --recursive
